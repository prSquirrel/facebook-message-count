name := """facebook-message-count"""

version := "0.0.1"

scalaVersion := "2.11.8"

//addCompilerPlugin("org.scalamacros" %% "paradise" % "2.1.0" cross CrossVersion.full)

resolvers ++= Seq(
//  "Sonatype OSS Snapshots" at
//      "https://oss.sonatype.org/content/repositories/snapshots",
//  "Sonatype OSS Releases" at
//		"https://oss.sonatype.org/content/repositories/releases"
)

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-native" % "3.3.0",
  "org.facebook4j" % "facebook4j-core" % "2.4.4"
)

fork in run := true
cancelable in Global := true

scalacOptions ++= Seq(
//	"-Xlint",
//	"-P:minibox:log",
//	"-Ywarn-dead-code",
//	"-Ywarn-value-discard",
//	"-Ywarn-numeric-widen",
//	"-Ywarn-unused",
//	"-Ywarn-unused-import",
//  "-unchecked",
//  "-deprecation",
//  "-feature",
//  "-language:implicitConversions",
//  "-language:higherKinds",
//  "-encoding", "UTF-8",
//  "-Xlog-implicits"
)
//mainClass in (Compile, run) := Some("")

javaOptions in run ++= Seq(
//	"-Xmx1G",
//	"-XX:+UseConcMarkSweepGC",
//	"-XX:+UseG1GC",
//	"-XX:MaxGCPauseMillis=15",
//	"-XX:+PrintGCDetails",
//	"-XX:+PrintGCTimeStamps",
//	"-verbose:gc"
)
