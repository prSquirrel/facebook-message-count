import facebook4j._
import facebook4j.auth._

import org.json4s._
import org.json4s.native.JsonMethods._

import java.io.{File, FileOutputStream}


object GraphExplorer {
  object StringToLong extends CustomSerializer[Long](format => ({ case JString(x) => x.toLong }, { case x: Long => JInt(x) }))
  implicit val formats = DefaultFormats + StringToLong
}

//get token from
//https://developers.facebook.com/tools/explorer/145634995501895
class GraphExplorer(accessToken: String) {
  import GraphExplorer._
  private[this] val facebook = new FacebookFactory().getInstance()
  //using default OAuth settings for Graph API Explorer tool
  facebook.setOAuthAppId("", "")
  facebook.setOAuthAccessToken(new AccessToken(accessToken))

  //IMPORTANT: Requires explicit type parameters when calling
  //otherwise, may fail silently at runtime
  def fqlQueryOpt[A](query: String, extract: JValue => JValue)(implicit formats: Formats, mf: Manifest[A]): Option[A] = {
    val json: JValue = parse(facebook.executeFQL(query).toString)
    extract(json).extractOpt[A]
  }

  def currentUserUid(): Option[Long] = {
    val query = "SELECT uid FROM user WHERE uid=me()"
    fqlQueryOpt[Long](query, _ \ "uid")
  }

  def uidToFriendName(uid: Long): Option[String] = {
    val query = s"SELECT name FROM user WHERE uid=$uid"
    fqlQueryOpt[String](query, _ \ "name")
  }

  def inboxMsgCount(): List[NamedThread] = {
    val query =
      """SELECT recipients, message_count
          FROM thread WHERE folder_id = 0 ORDER BY message_count DESC"""
    val threads =
      parse(facebook.executeFQL(query).toString)
        .extract[List[UidThread]]

    //remove uid of the current user from response
    //and transform UIDs into names
    threads.map { t: UidThread =>
      val filtered =
        currentUserUid().fold(t.recipients)(t.recipients - _)
      NamedThread(filtered.flatMap(uidToFriendName _), t.message_count)
    }
  }
}
