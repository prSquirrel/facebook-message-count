import java.nio.charset.StandardCharsets
import java.io.{File, FileOutputStream}

import GraphExplorer._

object Main {
  def main(args: Array[String]): Unit = {
    require(args.size == 2)
    val ge = new GraphExplorer(args(0))

    val result: List[NamedThread] = ge.inboxMsgCount()

    val stream = new FileOutputStream(new File(args(1)))
    try {
      result.foreach {
        case NamedThread(set, count) =>
          val str = s"${set.mkString(",")} = $count\n"
          stream.write(str.getBytes(StandardCharsets.UTF_8))
      }
    } finally { stream.close() }
  }
}
